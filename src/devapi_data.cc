#include "tango.h"

namespace Tango
{
    DeviceData::DeviceData()
    {
        Any any;
        any.tag = Any::ISNULL;
    }

    DeviceData::DeviceData(const DeviceData &dd)
    {
        any = dd.any;
        any.tag = dd.any.tag;
    }

    DeviceData &DeviceData::operator=(const DeviceData &dd)
    {
        return *this;
    }
    DeviceData &DeviceData::operator=(DeviceData &&dd)
    {
        return *this;
    }

    bool DeviceData::operator>>(short &datum)
    {
        bool ret = any.tag == Any::SHORT;
        if (ret)
        {
            datum = any.s;
        }
        return ret;
    }

    bool DeviceData::operator>>(unsigned short &datum)
    {
        bool ret = any.tag == Any::USHORT;
        if (ret)
        {
            datum = any.us;
        }
        return ret;
    }

    bool DeviceData::operator>>(DevLong &datum)
    {
        bool ret = any.tag == Any::DEVLONG;
        if (ret)
        {
            datum = any.devlong;
        }
        return ret;
    }

    bool DeviceData::operator>>(DevULong &datum)
    {
        bool ret = any.tag == Any::DEVULONG;
        if (ret)
        {
            datum = any.devulong;
        }
        return ret;
    }

    bool DeviceData::operator>>(DevLong64 &datum)
    {
        bool ret = any.tag == Any::DEVLONG64;
        if (ret)
        {
            datum = any.devlong64;
        }
        return ret;
    }

    bool DeviceData::operator>>(DevULong64 &datum)
    {
        bool ret = any.tag == Any::DEVULONG64;
        if (ret)
        {
            datum = any.devulong64;
        }
        return ret;
    }

    bool DeviceData::operator>>(float &datum)
    {
        bool ret = any.tag == Any::FLOAT;
        if (ret)
        {
            datum = any.f;
        }
        return ret;
    }

    bool DeviceData::operator>>(double &datum)
    {
        bool ret = any.tag == Any::DOUBLE;
        if (ret)
        {
            datum = any.d;
        }
        return ret;
    }

    bool DeviceData::operator>>(const char *&datum)
    {
        bool ret = any.tag == Any::CHAR_PTR;
        if (ret)
        {
            datum = any.c;
        }
        return ret;
    }

    bool DeviceData::operator>>(std::string *&datum)
    {
        bool ret = any.tag == Any::STRING_PTR;
        if (ret)
        {
            datum = any.str;
        }
        return ret;
    }

    bool DeviceData::operator>>(std::string &datum)
    {
        bool ret = any.tag == Any::STRING_PTR;
        if (ret)
        {
            datum = *any.str;
        }
        return ret;
    }

    std::ostream &operator<<(std::ostream &str, DeviceData &dd)
    {
        switch (dd.any.tag)
        {
        case DeviceData::Any::SHORT:
            str << dd.any.s;
            break;
        case DeviceData::Any::USHORT:
            str << dd.any.us;
            break;
        case DeviceData::Any::DEVLONG:
            str << dd.any.devlong;
            break;
        case DeviceData::Any::DEVULONG:
            str << dd.any.devulong;
            break;
        case DeviceData::Any::DEVLONG64:
            str << dd.any.devlong64;
            break;
        case DeviceData::Any::DEVULONG64:
            str << dd.any.devulong64;
            break;
        case DeviceData::Any::FLOAT:
            str << dd.any.f;
            break;
        case DeviceData::Any::DOUBLE:
            str << dd.any.d;
            break;
        case DeviceData::Any::CHAR_PTR:
            str << dd.any.c;
            break;
        case DeviceData::Any::STRING_PTR:
            str << dd.any.str;
            break;
        case DeviceData::Any::DEVBOOLEAN:
            str << dd.any.devboolean;
            break;
        case DeviceData::Any::DEVDOUBLE:
            str << dd.any.devdouble;
            break;
        case DeviceData::Any::DEVSTATE:
            str << dd.any.devstate;
            break;
        }
        return str;
    }
    //should throw an exception
    bool DeviceData::any_is_null()
    {
        return any.tag == Any::ISNULL;
    }
} // namespace Tango