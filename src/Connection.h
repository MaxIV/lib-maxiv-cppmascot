#ifndef _CONNECTION_H
#define _CONNECTION_H

#include "tango.h"
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

#include "grpcproto/device_server.grpc.pb.h"

using grpc::Channel;
using Tango::DeviceData;

namespace Tango
{
    class Connection
    {
    protected:
        std::string host;    // DS host
        std::string port;    // DS port
        int port_num;        // DS port (as number)
        std::string db_host; // DB host
        std::string db_port; // DB port
        int db_port_num;     // DB port (as number)

    public:
        virtual DeviceData command_inout(std::string &cmd_name);
        virtual DeviceData command_inout(const char *cmd_name) {std::string str(cmd_name);return command_inout(str);}
        virtual DeviceData command_inout(std::string &cmd_name, DeviceData &d_in);
        virtual DeviceData command_inout(const char *cmd_name,DeviceData &d_in) {std::string str(cmd_name);return command_inout(str,d_in);}
        virtual void command_inout_asynch(std::string &command);

        //should in the future somehow take a std::shared_ptr<MascotDeviceServer::Stub>
        Connection();
        //virtual ~Connection();
        Connection(const Connection &);
        Connection &operator=(const Connection &);
        ApiUtil &au;
        std::string &get_db_host() { return db_host; }
        std::string &get_db_port() { return db_port; }
        int get_db_port_num() { return db_port_num; }
        std::string &get_dev_host() { return host; }
        std::string &get_dev_port() { return port; }
        void connect(std::string &device_name);
    };
} // namespace Tango
#endif