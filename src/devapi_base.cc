
#include "tango.h"

using namespace grpc;
namespace Tango
{

    Connection::Connection() : au(ApiUtil::instance()) {}

    DeviceProxy::~DeviceProxy()
    {
    }

    // The original Connection::connect takes an CORBA::IOR, these do not exist in gRPC and should simply be substituted for the IP address.
    // device_name should be on the form domain/class/member
    void Connection::connect(std::string &device_name)
    {
        //bool retry = true;
        //bool connect_to_db = false;
        //retry = false;
        //while (retry)
        //{
        //}
        au.create_and_get_stub("0.0.0.0:50005");
    }

    DeviceData Connection::command_inout(std::string &command)
    {
        DeviceData data_in;
        return (command_inout(command, data_in));
    }

    DeviceData Connection::command_inout(std::string &command, DeviceData &data_in)
    {
        auto _stub = au.get_stub();
        DeviceData data_out; // is this needed according to the RFC?
        ClientContext context;
        mascot::CommandMessage msg;
        msg.set_commandname(command);
        mascot::CommandReply rsp;
        Status status = _stub->ExecuteCommand(&context, msg, &rsp);
        status.ok() ? std::cout << "command sent successfully!" << std::endl
                    : std::cout << rsp.error().message()        << std::endl;
        return data_out;
    }

    void Connection::command_inout_asynch(std::string &command)
    {
        auto _stub = au.get_stub();
        ClientContext context;
        mascot::CommandMessage msg;
        msg.set_commandname(command);
        mascot::CommandReply rsp;
        CompletionQueue cq;
        std::unique_ptr<ClientAsyncResponseReader<mascot::CommandReply>> rpc(_stub->AsyncExecuteCommand(&context, msg, &cq));
        Status status;
        rpc->Finish(&rsp, &status, (void*)1);
        void *got_tag;
        bool ok = false;
        cq.Next(&got_tag, &ok);
        if (ok && got_tag == (void *)1)
        {
        status.ok() ? std::cout << "command sent successfully!" << std::endl
                    : std::cout << rsp.error().message()        << std::endl;
        }
    }

    /**
     * only accepts intvalues for now.
     * Disregards the DeviceAttribute.
     */
    DeviceAttribute DeviceProxy::read_attribute(std::string &attr_str, DeviceAttribute &dev_attr)
    {
        auto _stub = au.get_stub();
        ClientContext context;
        mascot::ReadAttributeMessage msg;
        msg.set_attributename(attr_str);
        mascot::ReadAttributeReply rsp;
        Status status = _stub->ReadAttribute(&context, msg, &rsp);
        status.ok() ? std::cout << "read successful! " << rsp.data().value().intvalue() << std::endl
                    : std::cout << rsp.error().message() << std::endl;
        au.attr_to_device(std::move(rsp),dev_attr);
        return dev_attr;
    }

    void DeviceProxy::write_attribute(DeviceAttribute &attr_in)
    {
        auto _stub = au.get_stub();
        ClientContext context;
        mascot::WriteAttributeMessage msg;
        au.device_to_wam(std::move(attr_in),msg);
        mascot::WriteReply rsp;
        Status status = _stub->WriteAttribute(&context, msg, &rsp);
        (status.ok() && rsp.success()) ? std::cout << "write successful!" << std::endl
                                       : std::cout << rsp.error().message() << std::endl;
    }
    /**
     * Pings the devices and returns the time it took in microseconds
     */
    int DeviceProxy::ping()
    {
        auto _stub = au.get_stub();
        ClientContext context;
        mascot::PingMessage msg;
        mascot::PingReply rsp;
        auto start = std::chrono::steady_clock::now();
        Status status = _stub->Ping(&context, msg, &rsp);
        auto end = std::chrono::steady_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
        if (!status.ok())
        {
            elapsed = -1;
        }
        return elapsed;
    }

} // namespace Tango