/*
* example of a client using the TANGO C++ api.
*/
#include "tango.h"
using namespace Tango;
int main(unsigned int argc, char **argv)
{
  try
  {

    //
    // create a connection to a TANGO device
    //

    DeviceProxy *device = new DeviceProxy("sys/database/2");

    //
    // Ping the device
    //

    device->ping();

    //
    // Execute a command on the device and extract the reply as a string
    //

    string db_info;
    DeviceData cmd_reply;
    cmd_reply = device->command_inout("DbInfo");
    cmd_reply >> db_info;
    std::cout << "Command reply " << db_info << std::endl;

    //
    // Read a device attribute (string data type)
    //

    string spr;
    Tango::DeviceAttribute att_reply;
//    att_reply = device->read_attribute("StoredProcedureRelease");
//    att_reply >> spr;
//    std::cout << "Database device stored procedure release: " << spr << std::endl;
//  }
//  catch (DevFailed &e)
//  {
//    Except::print_exception(e);
//    exit(-1);
  }
}