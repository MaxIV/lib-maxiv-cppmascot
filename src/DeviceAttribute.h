#ifndef _DEVICEATTRIBUTE_H
#define _DEVICEATTRIBUTE_H

#include "grpcproto/attributes.grpc.pb.h"
#include "tango.h"
namespace Tango
{
    enum ValueType
    {
        kBoolValue,
        kIntValue,
        kDoubleValue,
        kStringValue,
        kNoValue,
    };

    enum AttrDataFormat
    {
        SCALAR,
        SPECTRUM,
        IMAGE,
        FMT_UNKNOWN
    };

    enum AttrQuality
    {
        ATTR_VALID,
        ATTR_INVALID,
        ATTR_ALARM,
        ATTR_CHANGING,
        ATTR_WARNING
    };

    class DeviceAttribute
    {
    public:
        enum except_flags
        {
            isempty_flag = 0,
            wrongtype_flag,
            failed_flag,
            unknown_format_flag,
            numFlags
        };

        //DeviceAttribute(const mascot::DeviceAttribute&);

        DeviceAttribute(const DeviceAttribute &);
        DeviceAttribute &operator=(const DeviceAttribute &);
        DeviceAttribute(DeviceAttribute &&);
        DeviceAttribute &operator=(DeviceAttribute &&);
        DeviceAttribute();
        DeviceAttribute(std::string &name, short val);

        DeviceAttribute(std::string &name, std::vector<short> &val);
        DeviceAttribute(std::string &name, std::vector<short> &val, int dim_x, int dim_y);

        DeviceAttribute(std::string &, DevLong);
        DeviceAttribute(std::string &, double);
        DeviceAttribute(std::string &, std::string &);
        DeviceAttribute(std::string &, const char *);
        DeviceAttribute(std::string &, float);
        DeviceAttribute(std::string &, bool);
        DeviceAttribute(std::string &, unsigned short);
        DeviceAttribute(std::string &, unsigned char);
        DeviceAttribute(std::string &, DevLong64);
        DeviceAttribute(std::string &, DevULong);
        DeviceAttribute(std::string &, DevULong64);
        DeviceAttribute(std::string &, DevState);
        //DeviceAttribute(std::string&, DevEncoded &);

        DeviceAttribute(std::string &, std::vector<DevLong> &);
        DeviceAttribute(std::string &, std::vector<double> &);
        DeviceAttribute(std::string &, std::vector<std::string> &);
        DeviceAttribute(std::string &, std::vector<float> &);
        DeviceAttribute(std::string &, std::vector<bool> &);
        DeviceAttribute(std::string &, std::vector<unsigned short> &);
        DeviceAttribute(std::string &, std::vector<unsigned char> &);
        DeviceAttribute(std::string &, std::vector<DevLong64> &);
        DeviceAttribute(std::string &, std::vector<DevULong> &);
        DeviceAttribute(std::string &, std::vector<DevULong64> &);
        DeviceAttribute(std::string &, std::vector<DevState> &);

        DeviceAttribute(std::string &, std::vector<DevLong> &, int, int);
        DeviceAttribute(std::string &, std::vector<double> &, int, int);
        DeviceAttribute(std::string &, std::vector<std::string> &, int, int);
        DeviceAttribute(std::string &, std::vector<float> &, int, int);
        DeviceAttribute(std::string &, std::vector<bool> &, int, int);
        DeviceAttribute(std::string &, std::vector<unsigned short> &, int, int);
        DeviceAttribute(std::string &, std::vector<unsigned char> &, int, int);
        DeviceAttribute(std::string &, std::vector<DevLong64> &, int, int);
        DeviceAttribute(std::string &, std::vector<DevULong> &, int, int);
        DeviceAttribute(std::string &, std::vector<DevULong64> &, int, int);
        DeviceAttribute(std::string &, std::vector<DevState> &, int, int);

        DeviceAttribute(const char *, short);
        DeviceAttribute(const char *, DevLong);
        DeviceAttribute(const char *, double);
        DeviceAttribute(const char *, std::string &);
        DeviceAttribute(const char *, const char *);
        DeviceAttribute(const char *, float);
        DeviceAttribute(const char *, bool);
        DeviceAttribute(const char *, unsigned short);
        DeviceAttribute(const char *, unsigned char);
        DeviceAttribute(const char *, DevLong64);
        DeviceAttribute(const char *, DevULong);
        DeviceAttribute(const char *, DevULong64);
        DeviceAttribute(const char *, DevState);
        //DeviceAttribute(const char *, DevEncoded &);

        DeviceAttribute(const char *, std::vector<short> &);
        DeviceAttribute(const char *, std::vector<DevLong> &);
        DeviceAttribute(const char *, std::vector<double> &);
        DeviceAttribute(const char *, std::vector<std::string> &);
        DeviceAttribute(const char *, std::vector<float> &);
        DeviceAttribute(const char *, std::vector<bool> &);
        DeviceAttribute(const char *, std::vector<unsigned short> &);
        DeviceAttribute(const char *, std::vector<unsigned char> &);
        DeviceAttribute(const char *, std::vector<DevLong64> &);
        DeviceAttribute(const char *, std::vector<DevULong> &);
        DeviceAttribute(const char *, std::vector<DevULong64> &);
        DeviceAttribute(const char *, std::vector<DevState> &);

        DeviceAttribute(const char *, std::vector<short> &, int, int);
        DeviceAttribute(const char *, std::vector<DevLong> &, int, int);
        DeviceAttribute(const char *, std::vector<double> &, int, int);
        DeviceAttribute(const char *, std::vector<std::string> &, int, int);
        DeviceAttribute(const char *, std::vector<float> &, int, int);
        DeviceAttribute(const char *, std::vector<bool> &, int, int);
        DeviceAttribute(const char *, std::vector<unsigned short> &, int, int);
        DeviceAttribute(const char *, std::vector<unsigned char> &, int, int);
        DeviceAttribute(const char *, std::vector<DevLong64> &, int, int);
        DeviceAttribute(const char *, std::vector<DevULong> &, int, int);
        DeviceAttribute(const char *, std::vector<DevULong64> &, int, int);
        DeviceAttribute(const char *, std::vector<DevState> &, int, int);

        virtual ~DeviceAttribute();
        std::string name;
        AttrQuality quality;
        AttrDataFormat data_format;
        ValueType valueType;
        int data_type;
        int dim_x;
        int dim_y;
        int w_dim_x;
        int w_dim_y;
        bool boolValue;
        std::int64_t intValue;
        double doubleValue;
        std::string stringValue;
        //TimeVal 			time;
    };

} // namespace Tango

#endif /* _DEVICEATTRIBUTE_H */