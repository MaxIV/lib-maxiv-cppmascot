#include "tango.h"

#define LOG(x) std::cout << x << std::endl

int main(int argc, char *argv[])
{
  std::string device_id("a/b/c"), command_name("cmd1"), attr_name("attr1"), attr_name_invalid("invalid");
  Tango::DeviceAttribute da;
  Tango::DeviceProxy dp("dp");
  dp.connect(device_id);
  dp.command_inout(command_name);
  dp.read_attribute(attr_name, da);
  LOG("read_attribute got: " << da.intValue);
  dp.read_attribute(attr_name_invalid, da);
  LOG("read_attribute got: " << da.intValue);
  LOG("ping took " << dp.ping() << " µs");
  da.intValue = 43;
  da.valueType = Tango::ValueType::kIntValue;
  da.name = attr_name;
  dp.write_attribute(da);
  dp.read_attribute(attr_name, da);
}