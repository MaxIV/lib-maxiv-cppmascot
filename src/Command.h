
#ifndef _COMMAND_H
#define _COMMAND_H

#include "tango.h"
namespace Tango
{

  class Command
  {
  public:
    Command(std::string s, Tango::CmdArgType in, Tango::CmdArgType out, std::function<void()> f);
    void execute();
    std::string get_lower_name();

  private:
    std::function<void()> foo;
    std::string name;
    std::string lower_name;
    Tango::CmdArgType in_type;
    Tango::CmdArgType out_type;
  };

} // namespace Tango
#endif // _COMMAND_H