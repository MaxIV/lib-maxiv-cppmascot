#include "tango.h"
namespace Tango {
    Command::Command(std::string s,
                     Tango::CmdArgType in,
                     Tango::CmdArgType out,
                     std::function<void()> f)
            : name(s), in_type(in), out_type(out), foo(f) {
        lower_name = name;
        std::transform(lower_name.begin(), lower_name.end(), lower_name.begin(), ::tolower);
    }
    void Command::execute(){
        foo();
    }

    std::string Command::get_lower_name(){ return lower_name; }


}