#include "tango.h"

namespace Tango
{
    DeviceAttribute::DeviceAttribute() : name("Name not set"), dim_x(0), dim_y(0), w_dim_x(0), w_dim_y(0),
     intValue(0), boolValue(false), doubleValue(0.0f), stringValue(""), quality(Tango::ATTR_INVALID),
     data_type(Tango::DATA_TYPE_UNKNOWN), data_format(Tango::FMT_UNKNOWN) {}
        //w_dim_x = 0;
        //w_dim_y = 0;
        //intValue = 0;
        //boolValue = false;
        //doubleValue = 0.0f;
        //stringValue = "";
        //time.tv_sec = 0;
        //time.tv_usec = 0;
        //time.tv_nsec = 0;
        //quality = Tango::ATTR_INVALID;
        //data_type = Tango::DATA_TYPE_UNKNOWN;
        //data_format = Tango::FMT_UNKNOWN;
        //d_state_filled = false;
        //exceptions_flags.set(failed_flag);
        //exceptions_flags.set(isempty_flag);
    
    DeviceAttribute::DeviceAttribute(DeviceAttribute const &)
    {

    }
    

    //DeviceAttribute::DeviceAttribute(const mascot::DeviceAttribute& da)
    //{

    //    name = std::move(da.name());
    //    //quality = parseMascotQuality(da.quality());

    //    switch (da.value().value_case())
    //    {
    //    case mascot::AttributeValue::kBoolValue:
    //        boolValue = std::move(da.value().boolvalue());
    //        break;
    //    case mascot::AttributeValue::kIntValue:
    //        intValue = std::move(da.value().intvalue());
    //        break;
    //    case mascot::AttributeValue::kDoubleValue:
    //        doubleValue = std::move(da.value().doublevalue());
    //        break;
    //    case mascot::AttributeValue::kStringValue:
    //        stringValue = std::move(da.value().stringvalue());
    //        break;
    //    }

    //}

    DeviceAttribute::~DeviceAttribute()
    {
    }
} // namespace Tango