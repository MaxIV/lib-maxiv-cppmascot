#include "tango.h"
#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/security/server_credentials.h>
#include "grpcproto/device_server.grpc.pb.h"

using grpc::ServerContext;
using grpc::Status;
using mascot::AttributeValue;
using mascot::DeviceAttribute;
using mascot::ReadAttributeMessage;
using mascot::ReadAttributeReply;
using mascot::WriteAttributeMessage;
using mascot::WriteReply;
using mascot::PingMessage;
using mascot::PingReply;

using mascot::CommandMessage;
using mascot::CommandReply;

class DeviceServer final : public mascot::MascotDeviceServer::Service
{
public:
    explicit DeviceServer()
    {
        auto f1 = []() {
            for (int i = 10; i > 0; --i)
            {
                std::cout << "cmd1-function executing... " << i << std::endl;
            }
            std::cout << "cmd1-function done." << std::endl;
        };
        auto f2 = []() { std::cout << "cmd2-function exec" << std::endl; };
        auto cmd1 = std::make_unique<Tango::Command>("cmd1", Tango::CmdArgType::DEV_VOID, Tango::CmdArgType::DEV_VOID, f1);
        auto cmd2 = std::make_unique<Tango::Command>("cmd2", Tango::CmdArgType::DEV_VOID, Tango::CmdArgType::DEV_VOID, f2);
        auto attr1 = std::make_pair("attr1", 42);
        attributes.emplace_back(attr1);
        commands.emplace_back(std::move(cmd1));
        commands.emplace_back(std::move(cmd2));
    }

    Status ExecuteCommand(ServerContext *context,
                          const CommandMessage *msg,
                          CommandReply *rsp) override
    {
        std::string command_lower(msg->commandname());
        std::transform(command_lower.begin(), command_lower.end(), command_lower.begin(), ::tolower);
        for (auto &cmd : commands)
        {
            if (cmd->get_lower_name() == command_lower)
            {
                cmd->execute();
                rsp->set_success(true);
                return Status::OK;
            }
        }
        rsp->set_success(false);
        return Status::CANCELLED;
    }

    Status ReadAttribute(ServerContext *context,
                         const ReadAttributeMessage *msg,
                         ReadAttributeReply *rsp) override
    {
        std::string attr_lower(msg->attributename());
        std::transform(attr_lower.begin(), attr_lower.end(), attr_lower.begin(), ::tolower);
        for (auto &attr : attributes)
        {
            if (attr.first == attr_lower)
            {
                rsp->mutable_data()->mutable_value()->set_intvalue(attr.second); 
                return Status::OK;
            }
        }
        return Status::CANCELLED;
    }

    //attributes are currently represented as pairs of (string,integer) hence the commented out line below
    Status WriteAttribute(ServerContext *context,
                          const WriteAttributeMessage *msg,
                          WriteReply *rsp) override
    {
        std::string attr_lower(msg->attributename());
        std::transform(attr_lower.begin(), attr_lower.end(), attr_lower.begin(), ::tolower);
        for (auto &attr : attributes)
        {
            if (attr.first == attr_lower)
            {
                switch (msg->value().value_case())
                {
                case mascot::AttributeValue::kBoolValue:
                    attr.second = msg->value().boolvalue();
                    break;
                case mascot::AttributeValue::kIntValue:
                    attr.second = msg->value().intvalue();
                    std::cout << attr.second << std::endl;
                    break;
                case mascot::AttributeValue::kDoubleValue:
                    attr.second = msg->value().doublevalue();
                    break;
                case mascot::AttributeValue::kStringValue:
                    //attr.second = msg->value().stringvalue(); 
                    break;
                default:
                    break;
                }
                rsp->set_success(true);
                return Status::OK;
            }
        }
        return Status::CANCELLED;
    }

    Status Ping(ServerContext *context,
                const PingMessage *msg,
                PingReply *rsp) override
    {
        std::cout << "Ping has arrived" << std::endl;
        //should register ping
        return Status::OK;
    }

    void StartDevice()
    {
        std::string server_address("0.0.0.0:50005");
        DeviceServer service;
        grpc::ServerBuilder builder;
        builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
        builder.RegisterService(&service);
        std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
        std::cout << "Server is alive and kicking on " << server_address << std::endl;
        server->Wait();
    }

private:
    std::vector<std::unique_ptr<Tango::Command>> commands;
    std::vector<std::pair<std::string, int64_t>> attributes;
};

int main(int argc, char **argv)
{
    DeviceServer ds;
    ds.StartDevice();
}