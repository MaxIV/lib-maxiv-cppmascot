# cppTango - corba + grpc


## Dependencies
+ grpc 
+ protobuf


## How to run
1. From inside the src folder run `make all`
2. in one terminal, from inside src, run `./testserver`
3. in another terminal, from insde src, run `./myDeviceProxy`


## What works
The project have aimed at keeping the file structure found in Tango, for the benefit of Tango developers to quickly get a grip on what has been done. So far a few parts have been implemented partially. These parts include the DeviceProxy, Device and ApiUtil. 
### DeviceProxy and Connection
+ `DeviceAttribute read_attribute(string&, DeviceAttribute&)`
+ `void write_attribute(DeviceAttribute&)`
+ `void command_inout(string&)`
+ `DeviceData command_inout_asynch(string&, DeviceData&)`
+ `int ping()`

### ApiUtil
+ `shared_ptr<mascot::MascotDeviceServer::Stub> create_and_get_stub(const string)`
+ `void set_stub(shared_ptr<mascot::MascotDeviceServer::Stub>)`
+ `shared_ptr<mascot::MascotDeviceServer::Stub> get_stub()`

### Device
Contains corresponding methods for all rpc calls in DeviceProxy, as well as two vectors, one for attributes and one for commands.

## How it works

### DeviceProxy
DeviceProxy uses the ApiUtil-singleton, to get access to the gRPC channel and stub. The DeviceProxy, and its parent, Connection, are defined in devapi_base.cc. The process for connecting a DeviceProxy to a device, and carrying out some simple rpc calls, is as follows:
1. `Tango::DeviceProxy dp("dp");` 

2. `dp.connect("a/b/c");` 
Calls ApiUtil's create_and_get_stub() with the hardcoded address of "0.0.0.0:50005". This instantiates a gRPC Stub with a connection to the hardcoded address, inside the ApiUtil instance. create_and_get_stub also returns a std::shared_ptr<mascot::MascotDeviceServer::Stub>, but this is discarded in connect, for now. 

3. `dp.read_attribute(string attr_str, DeviceAttribute da);` 
Calls ApiUtil's get_stub(). Its return value is of type std::shared_ptr<mascot::MascotDeviceServer::Stub>. This pointer is then used to carry out the remote procedure calls. Then the class which is responsible for getting the rpc over the wire is created: `mascot::ReadAttributeMessage msg;`. The msg is then updated with the parameter attr_str (corresponding to the attribute name) and sent to the corresponding Device. The return value is of type `mascot::ReadAttributeReply`, and carries something similar to a DeviceAttribute. To transform this into a proper `Tango::DeviceAttribute` the a ApiUtil method is used: `attr_to_device(mascot::ReadAttributeReply&&, DeviceAttribute&);`.

### Device
#### attributes
A device currently has access to a vector of type `vector<string, int64_t>`, where the string represents the attribute name, and the int64_t the value. This is clearly too rudimentary and should be switched to some proper class, as attributes can hold a lot more information. The `read_attribute` and `write_attribute` both have the sufficient logic to work with:
1. bools,
2. ints,
3. doubles,
4. strings
Until the attributes are represented in a more proper way than simple pairs, however, it is only possible to use the integer type, i.e. int64_t.

The DeviceAttribute class, which is used on the DeviceProxy side of things has no getters and setters yet. E.g. to carry out a write_attribute(DeviceAttribute), to update an attribute to the integer value 43, the user must manually set a few values in the DeviceAttribute, namely:
```
da.intValue = 43;
da.valueType = Tango::ValueType::kIntValue;
da.name = attr_name;
```
Then `dp.write_attribute(da);` can be executed.

#### commands
A device also has access to a `std::vector<std::unique_ptr<Tango::Command>>`, where all the commands are stored. Commands are, in essence, consisting of a name and a function pointer.


## What need to be done
- [ ] implement database
- [ ] make device id (a/b/c) mandatory for finding a device via database
- [ ] be able to add new attributes or commands to device, without specifying them in Device.cc
- [ ] Connection::Connect should look up the input string in a database rather than discarding it and connecting to a hardcoded IP address.
- [ ] getters and setters for DeviceAttribute



## Misc
On `make all` protoc puts all its newly generated files in the src folder directly. These are the ones used for building. The grpcproto folder inside the src folder is populated with protoc generated files as well. The only purpose of this folder is to keep my IDE's C++-engine error free, without having to bloat the src folder with a proper set of protoc-generated files. The `#include`'s concerning protoc-generated files do, however, refer to the grpcproto folder. This should be fixed for aesthetic reasons.